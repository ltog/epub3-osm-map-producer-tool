var map;
var ajaxRequest;
var plotlist;
var plotlayers=[];

function initMap() {
	// set up the map
	map = new L.Map('map');
	locationFilter = null;

	// create the tile layer with correct attribution
	var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
	var osmAttrib='<a href="http://www.openstreetmap.org/copyright">© OpenStreetMap</a>';
	var osm = new L.TileLayer(osmUrl, {minZoom: 0, maxZoom: 18, attribution: osmAttrib});		
	
	// add coordinates
	L.control.mousePosition({
		separator: " / ",
		position: "bottomleft",
		emptyString: "Move mouse over map to see coordinates",
		latFormatter: function(l){return "lat: "+l},
		lngFormatter: function(l){return "lon: "+l},
	}).addTo(map);

	// add scale
	L.control.scale({position: "topright"}).addTo(map);

	// add selection rectangle
	locationFilter = new L.LocationFilter({enable:true}).addTo(map);

	locationFilter.on("change", function(e){
		document.getElementById('maxLat').value = e.bounds._northEast.lat; //n
		document.getElementById('maxLon').value = e.bounds._northEast.lng; //e
		document.getElementById('minLat').value = e.bounds._southWest.lat; //s
		document.getElementById('minLon').value = e.bounds._southWest.lng; //w

		document.getElementById('startLat').value = (e.bounds._northEast.lat + e.bounds._southWest.lat)/2;
		document.getElementById('startLon').value = (e.bounds._northEast.lng + e.bounds._southWest.lng)/2;
		readSettings();
		updateNrOfTiles();
	});

	// start the map in Chur
	map.setView(new L.LatLng(46.85371, 9.51455),12);
	map.addLayer(osm);
}
