# epub3-osm-map-producer-tool

Project thesis "Embedding interactive maps derived from OpenStreetMap data into EPUB 3 based ebooks"

# Abstract

EPUB 3 is the widely accepted standard for enhanced ebooks, which contain interactive elements like quizzes or dynamic graphics. OpenStreetMap (OSM) is a freely available global database of crowdsourced geographical data, e.g. roads and buildings. In this thesis a JavaScript based software was imple- mented to support authors and publishers of enhanced ebooks in equipping their publications with interactive maps based on OSM data. To determine the needs and wants of users of such maps, a survey was developed and performed. The software supports producers by downloading map images (tiles) of a desired region and locally generating a downloadable zip archive of those, accompanied by appropriate metadata (EPUB manifest entries). Downloaded tiles are then integrated in the EPUB archive as single PNG images by the producer and can be navigated with the aid of the Leaflet library.

# Note regarding tile usage policies

Please respect the tile usage policy of your tile provider *strictly*! Relevant part of OpenStreetMap's tile usage policy https://operations.osmfoundation.org/policies/tiles/ (2018-01-08):

*Bulk downloading is strongly discouraged. Do not download tiles unnecessarily.*

*In particular, downloading significant areas of tiles at zoom levels 17 and higher for offline or later usage is forbidden without prior consultation with a System Administrator. These tiles are generally not available (cached) on the server in advance, and have to be rendered specifically for those requests, putting an unjustified burden on the available resources.*

*To avoid having your access blocked, please discuss your requirement with system administrators either via their wiki pages or on the IRC channel prior to starting.*
